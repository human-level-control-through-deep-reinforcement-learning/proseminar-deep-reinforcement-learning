#!pip install gym      uncomment if you didnt install gym, pygame and numpy before
#!pip install numpy    you may have to use 'pip3' instead of pip
#!pip install pygame

import gym
import numpy as np

envi = gym.make("Taxi-v3")
fullTrained = np.loadtxt('Q-Learning-QTable.csv')

episodes = -1
while episodes < 1:
    episodes = int(input("Please enter the number of episodes you want to watch (input should be greater than 0): "))

maxSteps = 30

for episode in range(episodes):
    state = envi.reset()
    episodeFinished = False
    sumReward = 0
    
    if(episode==0):
            print("Random play:") #for the first episode we show just a randomized play of the agent
    elif(episode==1):
        print("")
        print("Fulltrained AI play:") #Fulltrained means that the q-Table is based on the best q function calculated by the training

    for step in range(maxSteps):
        envi.render(mode="human")

        if(episode==0): #for the first episode we show just a randomized play of the agent
            action = envi.action_space.sample()
        else:
            action = np.argmax(fullTrained[state,:]) #choosing the "best" action from the q-Table
        
        newState, reward, episodeFinished, info = envi.step(action)

        sumReward = sumReward + reward

        if(episodeFinished or step == maxSteps -1):
            envi.render(mode="human")
            print("Reward: "+str(sumReward))
            break
        state = newState

envi.close()