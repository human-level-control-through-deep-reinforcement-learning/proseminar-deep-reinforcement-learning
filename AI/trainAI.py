#!pip install gym      uncomment if you didnt install gym and numpy before
#!pip install numpy    you may have to use 'pip3' instead of 'pip'
#!pip install pygame

import gym 
import numpy as np
import random

envi = gym.make("Taxi-v3")

#initalizing the q-Table
numberOfActions = envi.action_space.n
numberOfStates = envi.observation_space.n
dimension = (numberOfStates, numberOfActions)
qTable = np.zeros(dimension)

#initalizing the "learning parameters"
learningRate = 0.2             #changes how fast the Agent adopt new knowledge / changes the influence of new data to the q-Table
discountRate = 0.97             #value for discounting future rewards
explorationRate = 1             #value that shows how likely it is that the agent will explore the environment
minExplorationRate = 0.01       #minimal exploration rate in order to still make the agent explore the environment a little bit at the end of the training sequence
explorationReduction = 0.0005   #value for calculating the decrease of the exploration rate

maxSteps = 30
trainingEpisodes = 10000
save = False #just for saving the q-Table into a File set to True in order to train your own AI (q-Table/ look-up table)

#For each episode meaning each time our agent tries to play the game, the agent has to:
for episode in range(trainingEpisodes):
    state = envi.reset()
    episodeFinished = False

    #For each step meaning every step within one try to play the game, the agent has to:
    for step in range(maxSteps):

        if(random.uniform(0,1) > explorationRate): #exploration / exploitation trade-off
            action = np.argmax(qTable[state,:]) #takes the "best" action based on the allready learned knowledge
        else:
            action = envi.action_space.sample() #takes a random action to explore the environment 

        newState, reward, episodeFinished, info = envi.step(action) #applying the choosen action to the environment

        #refresh the q-Table with the Bellman equation
        qTable[state,action] = (1-learningRate)*qTable[state,action] + learningRate * (reward + discountRate*(np.max(qTable[newState,:])))
        
        #end the current episode if finished
        if(episodeFinished):
            break
        
        #refresh the current state
        state = newState

    #save our agent/policy/q-Table with half of the training episodes for comparison
    if(trainingEpisodes-(episode*2) == 0 and save):
        np.savetxt('Q-Learning-QTable2.csv', qTable, fmt='%4.6f', delimiter=' ')

    #recalculate the exploration rate 
    explorationRate = minExplorationRate + pow(2.71828,-(episode*explorationReduction))

#save the agent/policy/q-Table
if(save):
    np.savetxt('Q-Learning-QTable.csv', qTable, fmt='%4.6f', delimiter=' ')

envi.close()