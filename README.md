## Name
Reinforcement Learning via Q-Learning explained

## Description
This repository splits its self into two areas. The elaboration of Deep Reinforcement Learning and this program example.
The program is a simple example of Reinforcement Learning. It shows an Q-Learning algorithm to solve the simplest gym Reinforcement Learning problems. In this case you will see the Taxi game but you could use this code also for learning to deal with the other three 'Toy Text' environments.
If you are only interested in the end result of the trained AI playing Taxi-V3 you can look at Usage to see what you have to do for it. Else this projekt contains a trainAI.py file where the code to train an AI is in. The watchAI.py file which is just to watch the end result (the AI playing the game) and the two Q-Learning-Tables as csv files where the 'knowledge' of our AI is saved in (after training).

### Before Training:
![Video Vorschau](AI/Visual-Result/Before_training/random-taxi.gif)

### After Training:
![Video Vorschau](AI/Visual-Result/After_training/trained-taxi.gif)

## More advanced example
https://git.rwth-aachen.de/lennart.fanter/4-in-a-row-ai.
For those who are very interested in practical examples: I have also done a 4-in-a-row AI. There I used the way more advanced strategy of Double Duelling Deep Q-Networks (using four neural networks) which gets discussed in chapter 3 to let two AIs play against each other trying to outperform each other while learning game strategies. To take a closer look on my program I have made another git repository which definitely is more complex. It also may still be work in progress.

## Installation
First of all, you have to install python and pip (or rather python3 and pip3)
After this make sure you have installed gym and numpy via pip:
-pip install gym (Use gym 0.24.1!)
-pip install numpy
-pip install pygame
You could also uncomment the first three lines of code in both python files to install gym, pygame and numpy

## Usage
(Use gym 0.24.1!)
Go to your terminal and execute the file 'watchAI.py' by the command python3 watchAI.py
Make sure youre terminal's working directory is the same as the watchAI.py file is in else use the whole path of the file.
After this you will be asked to put a number of showing examples or rather how often the AI will play the game while you are able to watch it.
You will then see the agent play one game randomly and then the remaining games (you wanted to see regarding the input) with the learned knowledge  

## Authors and acknowledgment
AirFlow

## License
Feel free to use this code an play around.

## Project status
Finished